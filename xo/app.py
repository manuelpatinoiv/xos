import gamelogic as gl
import random
from gameboard import Gameboard
from os import system
from vocabulary import YES, NO, ANOTHER_TIME


def display_prompt(prompt_type: str) -> str:
    prompt = random.choice(prompt_type)
    return normalized(input(prompt))


def clear_screen() -> None:
    system('clear')


def print_board(board: Gameboard) -> None:
    clear_screen()
    print(board)


def play_again() -> bool:
    play_again = input("Great game! How about another round? [Y]/n ?") or 'yes'
    return True if play_again.lower() in YES else False


def normalized(input: str) -> str:
    return input.lower().strip()


if __name__ == '__main__':
    playing = True

    while playing:
        board = Gameboard()
        print_board(Gameboard())
        ready = normalized(input("Ready to play a game of Xs and Os? [Y]/n? ") or 'yes')

        if ready in NO:
            clear_screen()
            display_prompt(ANOTHER_TIME)
            break
        elif ready not in YES:
            # handle user input being incorrect
            continue

        clear_screen()
        winner = gl.start_game()
        print(f'Winner {winner}!')
        if play_again():
            continue
        else:
            display_prompt(ANOTHER_TIME)
            playing = False
