MOVES = {
    'tl': 'top_left',
    'tm': 'top_middle',
    'tr': 'top_right',
    'ml': 'middle_left',
    'mm': 'middle_middle',
    'mr': 'middle_right',
    'bl': 'bottom_left',
    'bm': 'bottom_middle',
    'br': 'bottom_right',
}


class Column:
    """
    Column object instantiated as a dictionary with Column().column
    Iterable like a list
    e.g. [i for i in Column()] = [0, 1, 2]
    """
    def __init__(self):
        self.column = {
            'top': '',
            'middle': '',
            'bottom': ''
        }

    def __getitem__(self, item):
        return list(self.column.values())[item]

    def __iter__(self):
        return iter([i for i in range(len(self.column))])

    def __len__(self):
        return len(self.column)


class Gameboard:
    """
    Tic Tac Toe Board
    Allows you to print the board to display the board at its current state
    and allows you to mark the board if the selected spot has not already been marked.
    """
    def __init__(self):
        self._board = {
            'left': Column().column,
            'middle': Column().column,
            'right': Column().column
        }
        self.spots_left = 9

    def mark_spot(self, spot, mark):
        section, column = spot.split('_')
        if self._is_markable(self._board[column][section]):
            self._board[column][section] = mark
            self.spots_left -= 1
            return True
        else:
            return False

    def _is_markable(self, board_spot):
        return not board_spot

    def __iter__(self):
        return iter(self._board.keys())

    def __getitem__(self, item):
        return self._board[item]

    def __repr__(self):
        board = ""
        for position in Column().column:
            for column in self._board:
                if column == 'middle':
                    board += f'|{self._board[column][position]:^3}|'
                else:
                    board += f'{self._board[column][position]:^3}'
            if position != 'bottom':
                board += '\n-----------\n'
        return board

    def columns(self):
        """
        Returns all columns represented as list of lists
        """
        return [list(self._board[column].values()) for column in self._board]

    def rows(self):
        """
        Returns all rows represented as list of lists
        """
        return [[column[i] for column in self.columns()] for i in range(len(Column()))]

    def diagonals(self):
        """
        Returns all diagonal lines represented as list of lists
        """
        diagonals = []
        diagonals.append([self.columns()[i][i] for i in range(len(Column()))])
        diagonals.append([self.columns()[i][len(Column()) - i - 1] for i in range(len(Column()))])

        return diagonals

    def find_winner(self):
        for i in [self.columns(), self.rows(), self.diagonals()]:
            for r in i:
                if len(set(r)) == 1:
                    return r[0]
