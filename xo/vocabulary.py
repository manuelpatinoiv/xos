YES = [
    'yes',
    'y',
    'si',
]

NO = [
    'no',
    'n',
    'nay',
]

PLAY_AGAIN = [
    "Great game! How about another round? ",
    "Let's go again! ",
]

ANOTHER_TIME = [
    "OK, let's play another time. ",
    "Great, let's play again soon! "
]

PLAYER_MARK = [
    "Where do you want to place your mark? ",
    "Good luck. Place your mark. ",
]

SPOT_TAKEN = [
    "That spot is taken, please choose a different spot. "
]

COMMAND_NOT_RECOGNIZED = [
    "I'm sorry, I didn't recognize what you entered. Please Try again ",
    "Did not compute, error! error! Please enter a valid response. "
]
