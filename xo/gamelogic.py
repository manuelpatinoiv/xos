from app import print_board, display_prompt
from gameboard import Gameboard, MOVES
from vocabulary import PLAYER_MARK, SPOT_TAKEN, COMMAND_NOT_RECOGNIZED
import random


def _computer_pick(board: Gameboard) -> None:
    # Mark is the capital letter O not number Zero
    MARK = 'O'
    valid = False

    while valid is False:
        computer_pick = random.choice(list(MOVES.keys()))
        choice = MOVES[computer_pick]
        valid = board.mark_spot(choice, MARK)


def user_pick(board: Gameboard, command_not_recognized: bool = False) -> None:
    try:
        MARK = 'X'
        if command_not_recognized:
            pick = display_prompt(COMMAND_NOT_RECOGNIZED)
        else:
            pick = display_prompt(PLAYER_MARK)
        choice = MOVES[pick]
        valid = board.mark_spot(choice, MARK)

        while not valid:
            pick = display_prompt(SPOT_TAKEN)
            valid = board.mark_spot(choice, MARK)
    except KeyError:
        print_board(board)
        user_pick(board, command_not_recognized=True)


def start_game():
    board = Gameboard()
    turn = 1
    winner = 'draw'

    while board.spots_left:
        print_board(board)
        user_pick(board)
        if turn >= 3 and board.find_winner():
            winner = 'user'
            break
        print_board(board)
        if not board.spots_left:
            return winner

        _computer_pick(board)
        if turn >= 3 and board.find_winner():
            winner = 'computer'
            break

        print_board(board)
        turn += 1
    print_board(board)

    return winner
