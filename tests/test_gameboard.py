import os
import sys
import unittest

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from xo.gameboard import Gameboard, Column  # noqa: 402


class TestColumn(unittest.TestCase):
    def test_get(self):
        column = Column()
        cc = column.column
        cc['top'] = 'X'
        cc['middle'] = 'O'
        cc['bottom'] = '2'
        self.assertEqual(column[0], 'X')
        self.assertEqual(column[1], 'O')
        self.assertEqual(column[2], '2')

    def test_len(self):
        self.assertEqual(len(Column()), 3)

    def test_iter(self):
        self.assertAlmostEqual([i for i in Column()], [0, 1, 2])


class TestGameboard(unittest.TestCase):
    def test_mark_spot(self):
        board = Gameboard()
        self.assertTrue(board.mark_spot('top_right', 'X'))

    def test_mark_spot_unavailable(self):
        board = Gameboard()
        board.mark_spot('top_right', 'X')
        self.assertFalse(board.mark_spot('top_right', 'X'))
