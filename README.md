# X's and O's


## Description
An over engineered way of writing tic tac toe. Let's you play tic tac toe in a terminal or docker container if you have docker installed. No need for a virtual environment since there are no external dependencies.


## Usage
For Docker users you have to build the image and run it. 
```bash
# build image
docker build -t xo .

# Run container
docker run -it xo
```

`python app.py`
```
tl | tm | tr
------------
ml | mm | mr
------------
bl | bm | br
```

Moves/Commands are the key values below:
{
    'tl': 'top_left',
    'tm': 'top_middle',
    'tr': 'top_right',
    'ml': 'middle_left',
    'mm': 'middle_middle',
    'mr': 'middle_right',
    'bl': 'bottom_left',
    'bm': 'bottom_middle',
    'br': 'bottom_right',
}

## Support
* Open Issue
* Email manuelpatinoiv@gmail.com for support or suggestions

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
